# Print打印


:::demo
default/print/index
:::



## Variables

| 参数 | 说明         | 类型   | 可选值 | 默认值 |
| ---- | ------------ | ------ | ------ | ------ |
| id   | dom元素的id  | String | -      | -      |
| html | html代码片段 | String | -      | -      |