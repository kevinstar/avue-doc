# Clipboard 复制剪切板


:::demo
default/clipboard/index
:::



## Variables

| 参数 | 说明       | 类型   | 可选值 | 默认值 |
| ---- | ---------- | ------ | ------ | ------ |
| text | 复制的文本 | String | -      | -      |