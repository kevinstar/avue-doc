# Keyboard 键盘组件



:::demo
default/keyboard/index
:::

## Variables

| 参数  | 说明     | 类型   | 可选值                     | 默认值  |
| ----- | -------- | ------ | -------------------------- | ------- |
| type  | 键盘类型 | String | default/number             | default |
| theme | 主题     | String | default/green/dark/classic | default |



