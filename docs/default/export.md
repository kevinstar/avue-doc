# Export excel导出/导出


``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里）-->
<script src="https://cdn.staticfile.org/FileSaver.js/2014-11-29/FileSaver.min.js"></script>
<script src="https://cdn.staticfile.org/xlsx/0.18.2/xlsx.full.min.js"></script>
```


## Excel导出

:::demo
default/export/index
:::



## Variables

| 参数   | 说明   | 类型   | 可选值 | 默认值               |
| ------ | ------ | ------ | ------ | -------------------- |
| title  | 标题   | String | -      | new Date().getTime() |
| column | 数据列 | Array  | -      | -                    |
| data   | 数据   | Array  | -      | -                    |



## Excel导入

:::demo
default/export/import
:::


