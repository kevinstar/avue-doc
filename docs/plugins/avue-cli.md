# cli后台模板

#### 基于 vue3.x + vite + element plus + vue-router + vuex，适配手机、平板、pc 的后台开源免费模板，希望减少工作量，帮助大家实现快速开发

## 在线体验
[在线体验](https://cli.avuejs.com)  

## 代码地址
[🎉 vue2.x + element-ui](https://gitee.com/smallweigit/avue-cli/tree/2.x/)

[⚡️ vue3.x + element-plus](https://gitee.com/smallweigit/avue-cli)


## 在线文档
[文档地址](https://www.kancloud.cn/smallwei/avue)
- 为保证文档实时更新及其多终端阅读，已托管[看云](https://www.kancloud.cn/smallwei/avue)
- 关于文档价格，**全部章节 59.99元**，关键部署章节、Q&A 等已免费提供给大家
- 关于文档收费问题，**即使没有此文档，我相信大多数同学都能运行起来avue-cli**，文档中书写花费了大量时间，也是对我们团队的支持。

## 功能
- 登录/注销
  - 用户名登录
  - 验证码登录
  - 第三方登陆(QQ,微信)
  - 人脸识别登录
- 错误的日志记录
- 灵活的10+多款主题自由配置(mac 主题)
- 路由权限、菜单权限、登录权限
- 本地化持久存储api
- 页面缓冲
- 面向全屏幕尺寸的响应式适配能力
- 对国际化的支持
- 自动刷新token等机制
- 全新的前端错误日志监控机制
- 前端路由动态服务端加载
- 无限极动态路由加载
- 模块的可拆卸化,达到开箱即用
- 更多。。。

## 页面展示
### 登陆
<img src='/images/cli/1.png' width="700">

### 主页
<img src='/images/cli/2.png' width="700">

### 炫酷主题
<img src='/images/cli/3.png' width="700">

### MAC主题
<img src='/images/cli/6.png' width="700">
<img src='/images/cli/7.png' width="700">

### 第三方网站
<img src='/images/cli/4.png' width="700">

### 全局搜索
<img src='/images/cli/5.png' width="700">
