# 搜索

## 搜索变量
:::demo  `search`变量为搜索的表单对象
crud/crud-search/params
:::

## 搜索按钮文字和图标配置
:::demo  `searchBtnText`和`emptyBtnText`为搜索和清空的文字`searchBtnIcon`和`emptyBtnIcon`为搜索和清空的图标
crud/crud-search/text
:::


## 搜索方法
:::demo  `search-change`为点击搜索后执行方法，`done`方法为关闭等待框,`search-reset`点击清空的执行方法
crud/crud-search/search-change
:::

## 搜索字段标题宽度
:::demo `searchLabelWidth`为标题的宽度，默认为`110`，可以配置到`option`下作用于全部,也可以单独配置每一项
crud/crud-search/labelWidth
:::


## 搜索字段排序
:::demo `searchOrder`为排序字段，不写默认为`0`搜索字段排序不影响表格顺序
crud/crud-search/order
:::

## 搜索过滤

:::demo
crud/crud-search/filter
:::

## 搜索验证规则

:::tip
具体参考[async-validator](https://github.com/yiminghe/async-validator)
::::

:::demo  配置验证字段的`searchRules`的数据对象即可，
crud/crud-search/rules
:::


## 搜索范围和宽度
:::demo  `searchSpan`搜索框的宽度，`searchRange`配置后可以开启范围搜索
crud/crud-search/span
```

## 搜索默认值
:::demo  `searchValue`为搜索的默认值
crud/crud-search/value
:::


## 局部展开收缩
:::demo  `searchIcon`是否启用功能按钮, `searchIndex`配置收缩展示的个数,默认为`2`个
crud/crud-search/index
:::


## 辅助提示语
:::demo  `searchTip`为提示的内容,`searchTipPlacement`为提示语的方向，默认为`bottom`
crud/crud-search/tip
:::

## 隐藏搜索折叠按钮
:::demo  `searchShowBtn`设置为:`false`
crud/crud-search/showBtn
:::

## 按钮是否单独成行
:::demo  前提的`searchMenuSpan`可以控制搜索按钮的长度
crud/crud-search/menuSpan
:::

## 定义类型
:::demo  `searchType`属性可以重新定义搜索框的类型
crud/crud-search/type
:::

## 自定义搜索卡槽
:::demo  `search`和`searchMenu`卡槽可以自定义搜索内容，不需要单独设置列`search`:`true`
crud/crud-search/searchMenu
:::

## 自定义列搜索
:::demo  列的`prop`加`-search`作为卡槽的名称即可开启自定义，
crud/crud-search/slot
:::


## 多级联动

:::demo  `cascader`为需要联动的子选择框`prop`值，可以写多个，形成一对多的关系,需要手动调用内部的`dicInit`方法去初始化表格联动数据
crud/crud-search/cascader
:::




## 单独日期搜索

:::demo 配置`dateBtn`为`true`即可激活,搜索后回调`date-change`方法
crud/crud-search/dateBtn
:::




