# 空状态

## 普通用法
:::demo `emptyText`属性可以配置空状态时的提示语
crud/crud-empty/base
:::


## 自定义空状态
:::demo 当然你也可以自定义`empty`卡槽
crud/crud-empty/slot
:::

