# 行编辑

可以批量对表格编辑和新增等操作

:::tip
如果进行其它操作可以参考[Form组件事件](/form/form-event.html)进行联动
::::

## 普通用法
:::demo  配置数据中`$cellEdit`为`true`即可开启首次编辑`addRowBtn`为行新增按钮，`cellBtn`设置为true则开启行编辑按钮，在配置中将编辑的字段设置`cell`为`true`,增删改查方法和`crud`组件使用一致，`rowKey`为主键的key，如果数据中存在主键，数据才会保存在表格中
crud/crud-cell/base
:::

## 内容自定义
:::demo 和普通的卡槽用法一致，可以参考[自定义列](/crud/crud-column.html#自定义列)和[自定义表单](/crud/crud-form.html#自定义表单)
crud/crud-cell/slot
:::


## 按钮自定义
:::demo  卡槽中的`row.$cellEdit`来判断是他的当前状态,`cancelBtn`为取消按钮
crud/crud-cell/button
:::

## 多级联动

:::demo
crud/crud-cell/cascader
:::


## 行单击编辑

:::demo 调用行单击事件，在调用内部`rowEdit`方法即可
crud/crud-cell/click
:::

## 行双击编辑

:::demo 调用行双击事件，在调用内部`rowEdit`方法即可
crud/crud-cell/dbClick
:::


