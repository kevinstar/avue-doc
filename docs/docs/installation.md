# 快速上手

### 介绍
通过本章节你可以了解到 Avue 的安装方法和基本使用姿势。


### 安装
##### 通过 npm 安装
在现有项目中使用 Avue 时，可以通过 npm 或 yarn 进行安装(需要先引入ElementPlus作为依赖支持)：

``` bash
npm i @smallwei/avue -S
yarn add @smallwei/avue -S
```

``` js
//需要先安装ElementPlus的依赖
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import {createApp} from 'vue'
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
const app =createApp({})
app.use(ElementPlus)
app.use(Avue);

```

##### 使用字典和上传组件需要引入axios

``` js
import {createApp} from 'vue'
import axios from 'axios'
const app = createApp({})
app.use(Avue,{axios})

```