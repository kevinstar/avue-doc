# Object对象用法

:::tip
 3.2.15+
::::

:::tip
 之前的版本option中column是数组的格式，3.2.15+后支持对象格式，操作更加方便
::::

:::demo `option`中的`column`可以配置成对象形式，用`prop`作为`key`
form/form-object
:::
