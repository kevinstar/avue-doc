# Time时间

## 基础用法

:::demo
form/form-time/base
:::

## 下拉框样式
```css
.popperClass .el-time-spinner__item{
  background-color: rgba(0,0,0,.2);
}
```

:::demo `popperClass`属性配置样式的`class`名字
form/form-time/popperClass
:::

## 固定时间点

:::demo，可设置`pickerOptions`中分别通过`start`、`end`和`step`指定可选的起始时间、结束时间和步长
form/form-time/step
:::


## 格式化

:::demo 使用`format`指定输入框的格式；使用`valueFormat`指定绑定值的格式。 
form/form-time/format
:::

## 时间范围

:::demo  
form/form-time/range
:::

## 固定时间范围
>若先选择开始时间，则结束时间内备选项的状态会随之改变
:::demo  
form/form-time/default
:::
