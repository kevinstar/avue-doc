# 表单自定义


## 自定义内容

:::demo  
form/form-slot/slot
:::


## 自定义标题

:::demo  
form/form-slot/label
:::

## 自定义错误提示

:::demo  
form/form-slot/error
:::


## 自定义按钮

:::demo  
form/form-slot/button
:::


## 自定义样式

```css
.formClassName{
  background-color: #409eff;
  .el-form-item__label{
    color:#fff;
  }
}
```

:::demo  `className`属性配置上样式的名字即可
form/form-slot/class
:::

## 引入三方组件


:::demo  
form/form-slot/component
:::


