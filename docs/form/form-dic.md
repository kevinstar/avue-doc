# 表单数据字典

:::tip
``` js
//使用字典需要引入axios
import axios from 'axios'
const app =createApp({})
app.use(Avue,{axios})

```
::::

## 本地字典

:::demo 本地字典只要配置`dicData`为一个`Array`数组即可，便会自动加载字典到对应的组件中，注意返回的字典中value类型和数据的类型必须要对应，比如都是字符串或者都是数字。
form/form-dic/local
:::

## 字段类型

:::demo `dataType`可以指定数据或者字典的数据类型
form/form-dic/type
:::

## 字段配置

:::demo 配置`props`对应的`label`和`value`即可，字典返回的是无任何层级包裹的，如果有层级需要配置`res`字段
form/form-dic/props
:::


## 网络字典

:::demo 网络字典不需要配置`dicData`属性，直接配置`dicUrl`字典接口即可，`dicMethod`指定请求类型,默认为get请求,`dicQuery`为请求携带的参数
form/form-dic/net
:::

## 字典格式化

:::demo `dicFormatter`为字典数据返回的执行函数，对字典处理完返回即可
form/form-dic/formatter
:::

## 禁止字典某项

:::demo 禁止的项目配置`disabled`为`true`
form/form-dic/disabled
:::


## 字典联动



:::demo  `cascader`为需要联动的子选择框`prop`值，填写多个就会形成一对多的关系,`cascaderIndex`设置默认选择第几项
form/form-dic/cascader
:::

## 修改类型
:::demo  调用内置方法`findObject`查找对应`prop`的属性序号 ,同时你也可以更新字典
form/form-dic/change
:::



## 修改数据
:::demo  和上面方法一样，只是再调用`updateDic`时不需要传新的字典，他会自己调用`dicUrl`去请求字典
form/form-dic/data
:::







