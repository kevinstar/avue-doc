# Slider滑块


## 基础用法

:::demo  通过将`type`属性的值指定为`slider`
form/form-slider/base
:::

## 离散值
>选项可以是离散的
:::demo  改变`step`的值可以改变步长，通过设置`showStops`属性可以显示间断点
form/form-slider/step
:::


## 带有输入框
>通过输入框设置精确数值
:::demo  设置`showInput`属性会在右侧显示一个输入框
form/form-slider/showInput
:::

##  范围选择
>支持选择某一数值范围
:::demo  设置`range`即可开启范围选择，此时绑定值是一个数组，其元素分别为最小边界值和最大边界值
form/form-slider/range
:::

##  竖向模式
:::demo  设置`vertical`可使 Slider 变成竖向模式，此时必须设置高度`height`属性
form/form-slider/vertical
:::


##  展示标记
:::demo  设置`marks`属性可以展示标记
form/form-slider/marks
:::


