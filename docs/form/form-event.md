# 表单组件事件

- `change`事件
- `click`事件
- `focus`事件
- `blur`事件
- `enter`事件

## 组件事件
:::demo  目前组件有5个事件`change`,`click`,`focus`,`blur`,`enter`
form/form-event/event
:::


## 组件对象

:::tip
- [input-table组件实际用法](/form/form-input-table.html)
::::


:::demo  
form/form-event/ref
:::

## 组件交互
:::demo  可以写判断逻辑，返回对应改变的对象属性
form/form-event/control
:::



