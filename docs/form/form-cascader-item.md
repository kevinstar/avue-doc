# 表单多级联动

:::tip
 `cascader`为需要联动的子选择框`prop`值，填写多个就会形成一对多的关系,`key`为上一级传递下来的值，你也可以直接写其他`form`的值
 ::::

## Select多级联动

:::demo 
form/form-cascader-item/select
:::


## Select+InputTable多级联动

:::demo 
form/form-cascader-item/table
:::



## Select+Radio多级联动

:::demo 
form/form-cascader-item/radio
:::



## Select+Input多级联动

:::demo 
form/form-cascader-item/input
:::


## 传递其他参数

:::demo 
form/form-cascader-item/params
:::