# 表单高级用法


## 表单初始化

:::demo
form/form-ajax/init
:::


## 配置项服务端加载
:::tip
- 这里没有走真真的服务器请求，而是做了一个模拟
::::

:::demo
form/form-ajax/ajax
:::


## 配置项切换

:::demo
form/form-ajax/change
:::

## 动态改变结构


:::demo  
form/form-ajax/option
:::
