# Checkbox多选框


## 基础用法
>由于选项默认可见，不宜过多，若选项过多，建议使用 Select 选择器。

:::demo  通过将`type`属性的值指定为`checkbox`,同时配置`dicData`为字典值
form/form-checkbox/base
:::

## 网络字典
>更多用法参考[表单数据字典](/form/form-dic)

:::demo 配置`dicUrl`指定后台接口的地址
form/form-checkbox/dic
:::


## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-checkbox/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-checkbox/disabled
:::

## 禁用选项

:::demo 返回的字典中数据配置`disabled`属性指定是否禁用
form/form-checkbox/disabled-item
:::


## 全选
:::demo  配置`all`为`true`
form/form-checkbox/all
:::



## 数量限制
:::demo  使用`min`和`max`属性能够限制可以被勾选的项目的数量。
form/form-checkbox/max
:::




## 按钮样式
:::demo  配置`button`为`true`
form/form-checkbox/button
:::



## 空心样式
:::demo  配置`border`为`true`
form/form-checkbox/border
:::
