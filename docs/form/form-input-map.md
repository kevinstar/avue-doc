# Map坐标选择器

``` html
<!-- 导入需要的包 （一定要放到index.html中的head标签里） -->
<!-- 高德地图api更新必须配合安全密钥使用 -->
<script>
  window._AMapSecurityConfig = {
    securityJsCode: 'xxxxx',
  }
</script>
<script type="text/javascript" src='https://webapi.amap.com/maps?v=1.4.11&key=xxxxxxxx&plugin=AMap.PlaceSearch'></script>
<script src="https://webapi.amap.com/ui/1.0/main.js?v=1.0.11"></script>
```

## 基础用法
:::demo  
form/form-input-map/base
:::

## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-input-map/value
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-input-map/disabled
:::

## 高德参数
:::demo  
form/form-input-map/params
:::

