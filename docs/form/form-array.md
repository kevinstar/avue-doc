
# Array数组框



## 基础用法

:::demo 通过将`type`属性的值指定为`array`
form/form-array/base
:::

## 默认值

:::demo `value`属性可以提供一个初始化的默认值
form/form-array/value
:::

## 最大限制

:::demo `limit`限制最大个数
form/form-array/limit
:::

## 禁用状态

:::demo 通过`disabled`属性指定是否禁用
form/form-array/disabled
:::

## 图片框

:::demo 通过将`type`属性的值指定为`img`
form/form-array/img
:::

## 超链接框

:::demo 通过将`type`属性的值指定为`url`
form/form-array/url
:::




