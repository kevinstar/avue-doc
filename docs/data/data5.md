
# DataDisplay 数据展示



:::demo
data/data5
:::

## Attributes

| 参数      | 说明       | 类型    | 可选值     | 默认值 |
| --------- | ---------- | ------- | ---------- | ------ |
| animation | 是否动画   | Boolean | false/true | true   |
| decimals  | 小数点位数 | Number  | —          | 0      |
| span      | 栅格数     | String  | —          | 8      |
| data      | 数据       | Array   | —          | -      |

