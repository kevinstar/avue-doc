---
home: true
heroImage: /images/logo.png
actions:
  - text: 开始学习(v3.2.20)💡
    link: /docs/installation/
    type: primar

  - text: 更新日志 📝
    link: /docs/changelog

  - text: 数据大屏 🔐
    link: https://data.avuejs.com
features:
- title: 易用灵活
  details: 已经会了 Vue3 Element-plus？即刻阅读文档开始使用吧。
- title: 丰富组件
  details: 包含了大量的常用组件库以及插件库。
- title: 高效兼容
  details: 兼容现在主流的浏览器，开箱即用的插件引入模式。


footer: MIT Licensed | Copyright © 2018-present Smallwei [网站备案号：蒙ICP备[19003764号-1]](https://beian.miit.gov.cn/)


---


### 快速上手

``` bash
# 安装
yarn add  @smallwei/avue -S # 或者：npm i @smallwei/avue -S

# 引入
import {createApp} from 'vue';
import Avue from '@smallwei/avue';
import '@smallwei/avue/lib/index.css';
const app =createApp({});
app.use(Avue);

```


### 问题反馈
[github问题反馈](https://github.com/nmxiaowei/avue/issues)  
[gitee问题反馈](https://gitee.com/smallweigit/avue/issues) 

### 交流群
- 添加客服微信，邀请你进群
- <img src="https://avuejs.com/images/wechat.jpg" width="150px"/>


### 友情链接

[Pig](https://pig4cloud.com/#/)
[Bladex](https://bladex.vip/#/)
[如梦技术](https://www.dreamlu.net/)
[XUNHUAN](https://xunhuan.me/)
[开源运营离黍](https://clenji.com/)
[ouxiang](https://ouxiang.me/)
[Guns](https://www.stylefeng.cn/)
[Jwchat](https://codegi.gitee.io/jwchatdoc/)
[JeeSite](https://jeesite.com/)
[驰骋低代码开发平台](http://www.ccflow.org/?from=avuejs)
<br/>

[云程低代码平台](http://www.yunchengxc.com)
